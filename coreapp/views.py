# views.py
import datetime
from flask import request
from coreapp import app

@app.route('/')
def index():
    page = """
    <!DOCTYPE html>
    <html lang="en-US">
    <head>
    <title>Hello World Page</title>
    <meta charset=utf-8">
    </head>
    <body>
    <h1>Enter a date</h1>
    <p>Enter a date below and we will find the day of the week.</p>
    <p><form action="/dow" method="GET">
    <select name="day">
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
    <option value="5">5</option>
    <option value="6">6</option>
    <option value="7">7</option>
    <option value="8">8</option>
    <option value="9">9</option>
    <option value="10">10</option>
    <option value="11">11</option>
    <option value="12">12</option>
    <option value="13">13</option>
    <option value="14">14</option>
    <option value="15">15</option>
    <option value="16">16</option>
    <option value="17">17</option>
    <option value="18">18</option>
    <option value="19">19</option>
    <option value="20">20</option>
    <option value="21">21</option>
    <option value="22">22</option>
    <option value="23">23</option>
    <option value="24">24</option>
    <option value="25">25</option>
    <option value="26">26</option>
    <option value="27">27</option>
    <option value="28">28</option>
    <option value="29">29</option>
    <option value="30">30</option>
    <option value="31">31</option>
    </select>
    <select name="month">
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
    <option value="5">5</option>
    <option value="6">6</option>
    <option value="7">7</option>
    <option value="8">8</option>
    <option value="9">9</option>
    <option value="10">10</option>
    <option value="11">11</option>
    <option value="12">12</option>
    </select>
    <select name="year">
    <option value="2014">2014</option>
    <option value="2015">2015</option>
    <option value="2016">2016</option>
    <option value="2017">2017</option>
    <option value="2018">2018</option>
    <option value="2019">2019</option>
    <option value="2020">2020</option>
    </select>
    <input type="submit" />
    </form>

    </p>
    </body>
    </html>
    """
    return page

@app.route('/dow', methods=['GET'])
def dow():
    day = int(request.args.get("day"))
    month = int(request.args.get("month"))
    year = int(request.args.get("year"))
    weekday = datetime.date(year, month, day).weekday()
    if weekday == 0:
        weekday = "Monday"
    if weekday == 1:
        weekday = "Tuesday"
    if weekday == 2:
        weekday = "Wednesday"
    if weekday == 3:
        weekday = "Thursday"
    if weekday == 4:
        weekday = "Friday"
    if weekday == 5:
        weekday = "Saturday"
    if weekday == 6:
        weekday = "Sunday"

    page = """
    <!DOCTYPE html>
    <html lang="en-US">
    <head>
    <title>Day of the week.</title>
    <meta charset=utf-8">
    </head>
    <body>
    <h1>Day of the Week</h1>
    <p>%s</p>
    </body>
    </html>
    """ % weekday

    try:
        weekday = datetime.date(year, month, day).weekday()
    except Exception as e:
        error_msg = str(e)

    return page

@app.errorhandler(500)
def bad_date(error_msg):
    error = "Error: Missing, incomplete, or incorrect input."
    type_of_error = error_msg

    page = """
    <!DOCTYPE html>
    <html lang="en-US">
    <head>
    <title>Day of the week.</title>
    <meta charset=utf-8">
    </head>
    <body>
    <h1>Day of the Week</h1>
    <p>%s<br>%s</p>
    </body>
    </html>
    """ % (error, type_of_error)

    return page
